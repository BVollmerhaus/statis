# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]


## [1.1.0] - 2020-12-11
### Added
* General monitoring support (`-m`) to alert on changes to a notifier's
  measurement or at a set threshold
* Monitoring of used memory and swap
* Monitoring of remaining battery charge
* Monitoring of CPU usage and frequency


## [1.0.0] - 2020-06-04
**Initial Release**


[Unreleased]: ../../compare/v1.1.0...master
[1.1.0]: ../../compare/v1.0.0...v1.1.0
[1.0.0]: ../../tags/v1.0.0

"""
Test cases for the date notifier.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest
from freezegun import freeze_time

from statis.modules import date


@pytest.fixture
def date_notifier() -> date.Date:
    return date.Date()


@freeze_time('2020-03-20')
def test_run_shows_date_in_iso_format(date_notifier):
    notification = date_notifier.run()
    assert notification.content == '2020-03-20'


@freeze_time('2020-03-20')
def test_run_shows_date_in_custom_format_if_specified(date_notifier):
    date_notifier.parse_args(['-f', '%B %d'])
    notification = date_notifier.run()
    assert notification.content == 'March 20'


def test_run_shows_custom_notification_title_if_specified(date_notifier):
    date_notifier.parse_args(['-t', 'Today is the'])
    notification = date_notifier.run()
    assert notification.title == 'Today is the'

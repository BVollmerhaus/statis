"""
Test cases for the time notifier.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest
from freezegun import freeze_time

from statis.modules.time import Time


@pytest.fixture
def time_notifier() -> Time:
    return Time()


@freeze_time('14:15')
def test_run_shows_local_time_in_24h_notation(time_notifier):
    notification = time_notifier.run()
    assert notification.content == '14:15'


@freeze_time('14:15')
def test_run_shows_local_time_in_12h_notation_if_flag_set(time_notifier):
    time_notifier.parse_args(['--12'])
    notification = time_notifier.run()
    assert notification.content.upper() == '02:15 PM'


@freeze_time('14:15')
def test_run_shows_time_in_specified_timezone(time_notifier):
    time_notifier.parse_args(['-t', 'Asia/Tokyo'])
    notification = time_notifier.run()
    assert notification.content == '23:15'


def test_run_includes_specified_timezone_in_title(time_notifier):
    time_notifier.parse_args(['-t', 'Antarctica/South_Pole'])
    notification = time_notifier.run()
    assert notification.title == 'Time (South Pole)'


def test_parse_args_exits_if_timezone_not_exists(time_notifier):
    with pytest.raises(SystemExit):
        time_notifier.parse_args(['-t', 'Mars/Alba_City'])

"""
Common test cases for memory notifiers.

All memory notifiers (that derive from :class:`.MemoryNotifier`)
share common CLI arguments and formatting. They mostly differ in
the measurement (used, free, etc.) they display. To test all the
shared functionality without duplicating tests, these test cases
are run against multiple notifiers.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

from typing import NamedTuple

import pytest

from statis.modules import memory
from statis.notifier import NotifierError


class NotifierTestTarget(NamedTuple):
    """
    :cvar notifier:     A :class:`.MemoryNotifier` implementation
    :cvar measure_func: The FQN of the function used by the notifier
                        to retrieve its respective memory measurement
    :cvar total_func:   The FQN of the function used by the notifier
                        to retrieve its memory measurement's total
    """
    notifier: memory.MemoryNotifier
    measure_func: str
    total_func: str


@pytest.fixture(params=[
    (memory.Used(),
     'statis.hardware.memory.used',
     'statis.hardware.memory.total'),
    (memory.Free(),
     'statis.hardware.memory.available',
     'statis.hardware.memory.total'),
    (memory.UsedSwap(),
     'statis.hardware.memory.used_swap',
     'statis.hardware.memory.total_swap'),
    (memory.FreeSwap(),
     'statis.hardware.memory.free_swap',
     'statis.hardware.memory.total_swap'),
], ids=['used', 'free', 'used-swap', 'free-swap'])
def test_target(request):
    return NotifierTestTarget(request.param[0], request.param[1],
                              request.param[2])


def test_run_shows_memory_measure_in_gb(mocker, test_target):
    mocker.patch(test_target.measure_func, return_value=1048596000)
    notification = test_target.notifier.run()
    assert notification.content == '1.05 GB'


def test_run_shows_memory_measure_in_specified_unit(mocker, test_target):
    mocker.patch(test_target.measure_func, return_value=1048596000)
    test_target.notifier.parse_args(['-u', 'GiB'])
    notification = test_target.notifier.run()
    assert notification.content == '0.98 GiB'


def test_run_shows_memory_measure_of_total_if_flag_set(mocker, test_target):
    mocker.patch(test_target.measure_func, return_value=1048596000)
    mocker.patch(test_target.total_func, return_value=12442005504)
    test_target.notifier.parse_args(['-t'])
    notification = test_target.notifier.run()
    assert notification.content == '1.05 / 12.44 GB'


def test_run_shows_memory_measure_as_percentage(mocker, test_target):
    mocker.patch(test_target.measure_func, return_value=1048596000)
    mocker.patch(test_target.total_func, return_value=12442005504)
    test_target.notifier.parse_args(['-u', '%'])
    notification = test_target.notifier.run()
    assert notification.content == '8.4 %'


def test_run_raises_exception_if_unit_invalid(test_target):
    test_target.notifier.parse_args(['-u', 'XB'])
    with pytest.raises(NotifierError):
        test_target.notifier.run()

"""
Test cases for the CPU usage notifier.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest

from statis.modules import cpu


@pytest.fixture(autouse=True)
def patch_core_count(mocker):
    """Patch core count to ensure all tests use a valid core arg."""
    mocker.patch('statis.hardware.cpu.core_count', return_value=16)


@pytest.fixture
def usage_notifier() -> cpu.Usage:
    return cpu.Usage()


def test_run_shows_total_usage(mocker, usage_notifier):
    mocker.patch('statis.hardware.cpu.usage', return_value=12.5)
    notification = usage_notifier.run()
    assert notification.content == '12.5 %'


def test_run_shows_usage_of_specified_core(mocker, usage_notifier):
    usage_mock = mocker.patch('statis.hardware.cpu.usage', return_value=100)
    usage_notifier.parse_args(['-c', '8'])
    notification = usage_notifier.run()
    usage_mock.assert_called_with(core_num=8)
    assert notification.content == '100 %'


def test_run_includes_specified_core_in_title(mocker, usage_notifier):
    mocker.patch('statis.hardware.cpu.usage')
    usage_notifier.parse_args(['-c', '8'])
    notification = usage_notifier.run()
    assert notification.title == 'CPU Usage (Core 8)'


def test_parse_args_exits_if_core_num_below_0(usage_notifier):
    with pytest.raises(SystemExit):
        usage_notifier.parse_args(['-c', '-1'])


def test_parse_args_exits_if_core_num_higher_than_present(usage_notifier):
    with pytest.raises(SystemExit):
        usage_notifier.parse_args(['-c', '17'])

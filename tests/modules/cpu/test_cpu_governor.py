"""
Test cases for the CPU governor notifier.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest

from statis.modules import cpu


@pytest.fixture
def governor_notifier() -> cpu.Governor:
    return cpu.Governor()


def test_run_shows_active_governor(mocker, governor_notifier):
    mocker.patch('statis.hardware.cpu.governor', return_value='eurobeat')
    notification = governor_notifier.run()
    assert notification.content == 'eurobeat'  # Can't get any faster than this

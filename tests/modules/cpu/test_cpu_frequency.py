"""
Test cases for the CPU frequency notifier.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest

from statis.modules import cpu


@pytest.fixture
def frequency_notifier() -> cpu.Frequency:
    return cpu.Frequency()


def test_run_shows_freq_in_mhz_if_below_1000(mocker, frequency_notifier):
    mocker.patch('statis.hardware.cpu.frequency', return_value=999)
    notification = frequency_notifier.run()
    assert notification.content == '999.0 MHz'


def test_run_shows_freq_in_ghz_if_equal_or_above_1000(mocker,
                                                      frequency_notifier):
    mocker.patch('statis.hardware.cpu.frequency', return_value=1000)
    notification = frequency_notifier.run()
    assert notification.content == '1.00 GHz'


@pytest.mark.parametrize('frequency, expected_content', [
    (500.0, '500.0 MHz'),
    (860.512, '860.5 MHz'),
])
def test_run_shows_freq_with_one_decimal_if_mhz(mocker, frequency_notifier,
                                                frequency, expected_content):
    mocker.patch('statis.hardware.cpu.frequency', return_value=frequency)
    notification = frequency_notifier.run()
    assert notification.content == expected_content


@pytest.mark.parametrize('frequency, expected_content', [
    (1856.0, '1.86 GHz'),
    (4200.0, '4.20 GHz'),
])
def test_run_shows_freq_with_two_decimals_if_ghz(mocker, frequency_notifier,
                                                 frequency, expected_content):
    mocker.patch('statis.hardware.cpu.frequency', return_value=frequency)
    notification = frequency_notifier.run()
    assert notification.content == expected_content

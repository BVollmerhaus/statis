"""
Test cases for the battery charge notifier.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest

from statis.modules import battery
from statis.notifier import NotifierError


@pytest.fixture(autouse=True)
def patch_power_supply_path(mocker, datadir):
    mocker.patch('statis.modules.battery.PS_SYSFS_PATH',
                 datadir / 'power_supply')


@pytest.fixture
def charge_notifier() -> battery.Charge:
    return battery.Charge()


def test_run_shows_charge_of_first_found_battery(charge_notifier):
    notification = charge_notifier.run()
    assert notification.content == '100 %'


def test_run_shows_charge_of_specified_battery(charge_notifier):
    charge_notifier.parse_args(['-b', '1'])
    notification = charge_notifier.run()
    assert notification.content == '50 %'


def test_run_includes_battery_num_in_title_if_show_flag_set(charge_notifier):
    charge_notifier.parse_args(['-b', '1', '-n'])
    notification = charge_notifier.run()
    assert notification.title == 'Battery Charge (1)'


def test_run_raises_exception_if_battery_not_exists(charge_notifier):
    charge_notifier.parse_args(['-b', '2'])
    with pytest.raises(NotifierError):
        charge_notifier.run()


def test_run_raises_exception_if_no_batteries_found(mocker, datadir,
                                                    charge_notifier):
    mocker.patch('statis.modules.battery.PS_SYSFS_PATH',
                 datadir / 'power_supply_no_batteries')
    with pytest.raises(NotifierError):
        charge_notifier.run()


def test_parse_args_exits_if_battery_num_below_0(charge_notifier):
    with pytest.raises(SystemExit):
        charge_notifier.parse_args(['-b', '-1'])

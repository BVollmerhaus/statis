"""
Test cases for the :class:`.Monitor` mixin.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

from typing import List, NoReturn
from unittest.mock import MagicMock

import pytest

from statis.monitor import Monitor, Threshold, ThresholdUnitError
from statis.notification import Notification
from statis.notifier import Notifier


class MockMonitor(Notifier, Monitor):
    def run(self) -> Notification:
        pass

    def monitor(self) -> NoReturn:
        pass

    def valid_threshold_units(self) -> List[str]:
        return ['GB', '%']


def test_configure_monitor_raises_exception_if_unit_unsupported():
    monitor = MockMonitor()
    with pytest.raises(ThresholdUnitError):
        monitor.configure_monitor(lambda: None, Threshold(4, 'bad'))


@pytest.mark.parametrize('t_type, true_value, false_value', [
    (Threshold.Type.EQUAL, 4, 0),
    (Threshold.Type.ABOVE, 5, 4),
    (Threshold.Type.BELOW, 3, 4),
    (Threshold.Type.ABOVE_EQ, 4, 3),
    (Threshold.Type.ABOVE_EQ, 5, 3),
    (Threshold.Type.BELOW_EQ, 4, 5),
    (Threshold.Type.BELOW_EQ, 3, 5),
])
def test_check_threshold_runs_callback_if_threshold_reached(
        t_type: Threshold.Type, true_value: int, false_value: int):
    monitor = MockMonitor()
    mock_callback = MagicMock()
    monitor.configure_monitor(mock_callback, Threshold(4, 'GB', t_type))

    monitor.check_threshold(false_value)
    mock_callback.assert_not_called()

    monitor.check_threshold(true_value)
    mock_callback.assert_called_once()


def test_check_threshold_runs_callback_on_all_changes_if_no_threshold_set():
    monitor = MockMonitor()
    mock_callback = MagicMock()
    monitor.configure_monitor(mock_callback, None)

    monitor.check_threshold(1)
    mock_callback.assert_not_called()  # Not invoked on first call

    monitor.check_threshold(2)
    assert mock_callback.call_count == 1
    monitor.check_threshold(1)
    assert mock_callback.call_count == 2


def test_check_threshold_does_not_run_callback_repeatedly_for_same_threshold():
    monitor = MockMonitor()
    mock_callback = MagicMock()
    monitor.configure_monitor(mock_callback, Threshold(4, 'GB'))

    # Callback not invoked multiple times per threshold excess
    monitor.check_threshold(5)
    assert mock_callback.call_count == 1
    monitor.check_threshold(6)
    assert mock_callback.call_count == 1

    # Can be invoked again after falling below threshold once
    monitor.check_threshold(3)
    assert mock_callback.call_count == 1
    monitor.check_threshold(5)
    assert mock_callback.call_count == 2


def test_check_threshold_runs_callback_with_percentage_as_threshold():
    monitor = MockMonitor()
    mock_callback = MagicMock()
    monitor.configure_monitor(mock_callback, Threshold(25, '%'))

    monitor.check_threshold(50, 200)
    assert mock_callback.call_count == 0
    monitor.check_threshold(51, 200)
    assert mock_callback.call_count == 1


def test_check_threshold_raises_exception_if_unit_percent_but_no_total_given():
    monitor = MockMonitor()
    monitor.configure_monitor(lambda: None, Threshold(25, '%'))
    with pytest.raises(TypeError):
        monitor.check_threshold(50)


def test_check_threshold_raises_exception_if_unit_percent_but_total_is_0():
    monitor = MockMonitor()
    monitor.configure_monitor(lambda: None, Threshold(25, '%'))
    with pytest.raises(ValueError):
        monitor.check_threshold(50, 0)

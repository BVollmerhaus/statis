"""
Test cases for module import and retrieval of contained notifiers.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pathlib

import pytest

from statis import module
from statis.notifier import Notifier


@pytest.fixture
def module_path(shared_datadir) -> pathlib.Path:
    return shared_datadir / 'modules'


def test_get_notifier_returns_notifier_class(module_path):
    notifier_class = module.get_notifier(
        'NotifierA', module_path / 'file_module.py')
    assert notifier_class is not None
    assert issubclass(notifier_class, Notifier)


def test_get_notifier_raises_exception_if_path_not_exists(module_path):
    with pytest.raises(FileNotFoundError):
        module.get_notifier('NotifierA', module_path / 'non-existent.py')


def test_get_notifier_returns_none_if_class_not_exists(module_path):
    assert module.get_notifier(
        'NonExistentClass', module_path / 'file_module.py') is None


def test_get_notifier_returns_none_if_class_not_a_notifier(module_path):
    assert module.get_notifier(
        'NotANotifier', module_path / 'file_module.py') is None


def test_list_notifiers_returns_all_notifier_names(module_path):
    notifiers = module.list_notifiers(module_path / 'file_module.py')
    assert notifiers == ['file-module', 'notifier-a', 'notifier-b']

"""
Test cases for conversion between measurement units.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest

from statis import units


@pytest.mark.parametrize('no_of_bytes, unit, expected', [
    (-1_000_000, 'KB', -1000),
    (1_000_000, 'KB', 1000),
    (1_000_000, 'MB', 1),
    (1_000_000, 'GB', 0.001),
    (1_000_000_000, 'YB', 1e-15),
])
def test_bytes_to_unit_with_si(no_of_bytes: int, unit: str, expected: float):
    assert units.bytes_to_unit(no_of_bytes, unit) == expected


@pytest.mark.parametrize('no_of_bytes, unit, expected', [
    (-1_000_000, 'KiB', -976.5625),
    (1_000_000, 'KiB', 976.5625),
    (1_000_000, 'MiB', pytest.approx(0.953674316)),
    (1_000_000, 'GiB', pytest.approx(0.000931322575)),
    (1_000_000_000, 'YiB', 8.271806125530277e-16),
])
def test_bytes_to_unit_with_iec(no_of_bytes: int, unit: str, expected: float):
    assert units.bytes_to_unit(no_of_bytes, unit) == expected


@pytest.mark.parametrize('unit', ['', 'XB', 'KiiB'])
def test_bytes_to_unit_raises_exception_if_unit_invalid(unit: str):
    with pytest.raises(ValueError):
        units.bytes_to_unit(1_000_000, unit)


@pytest.mark.parametrize('celsius, expected_fahrenheit', [
    (-6.5, 20.3),
    (0, 32),
    (34.5, 94.1),
    (80, 176),
])
def test_celsius_to_fahrenheit(celsius: float, expected_fahrenheit: float):
    assert units.celsius_to_fahrenheit(celsius) == expected_fahrenheit

"""
Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import subprocess
import sys

import pytest

from statis.hardware import cpu


@pytest.mark.integration
@pytest.mark.skipif(sys.platform.startswith('win'),
                    reason='getconf not available on Windows')
def test_core_count_returns_number_of_logical_cpus():
    logical_core_count: bytes = subprocess.check_output(
        ['getconf', '_NPROCESSORS_ONLN'])
    assert cpu.core_count() == int(logical_core_count)


def test_governor_returns_scaling_governor_for_cpu0(fs):
    fs.create_file('/sys/devices/system/cpu/cpu0/cpufreq/'
                   'scaling_governor', contents='onfire')
    assert cpu.governor() == 'onfire'

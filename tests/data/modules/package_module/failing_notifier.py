from statis.notification import Notification
from statis.notifier import Notifier, NotifierError


class FailingNotifier(Notifier):
    def run(self) -> Notification:
        raise NotifierError('Could not build notification.')

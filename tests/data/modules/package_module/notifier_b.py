from statis.notification import Notification
from statis.notifier import Notifier


class NotifierB(Notifier):
    def run(self) -> Notification:
        return Notification('Title', 'Body')

from statis.notification import Notification
from statis.notifier import Notifier


class PackageModule(Notifier):
    """
    A notifier whose file and class name matches its containing
    module, which allows it to be run with only the module name.
    """

    def run(self) -> Notification:
        return Notification('Title', 'Body')

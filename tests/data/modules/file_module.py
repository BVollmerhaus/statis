from statis.notification import Notification
from statis.notifier import Notifier


class NotifierA(Notifier):
    def run(self) -> Notification:
        return Notification('Title', 'Body')


class NotifierB(Notifier):
    def run(self) -> Notification:
        return Notification('Title', 'Body')


class FileModule(Notifier):
    """
    A notifier whose class name matches its containing module,
    which allows it to be run specifying only the module name.
    """

    def run(self) -> Notification:
        return Notification('Title', 'Body')


class NotANotifier:
    def run(self) -> Notification:
        return Notification('Title', 'Body')

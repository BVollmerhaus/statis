"""
Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""


def pytest_configure(config):
    config.addinivalue_line(
        'markers', 'integration: mark a test as calling external '
                   'utils and/or interacting with system hardware'
    )

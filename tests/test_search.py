"""
Test cases for the search module.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest

from statis import search


@pytest.fixture(autouse=True)
def patch_search_paths(mocker, shared_datadir):
    """Replace module search paths with test data directory."""
    mocker.patch('statis.search.SEARCH_PATHS', [shared_datadir / 'modules'])


def test_find_notifier_in_module_implemented_as_file():
    assert search.find_notifier('file-module', 'notifier-a') is not None


def test_find_notifier_in_module_implemented_as_package():
    assert search.find_notifier('package-module', 'notifier-a') is not None


def test_find_notifier_by_only_module_name_if_identical():
    assert search.find_notifier('file-module', '') is not None
    assert search.find_notifier('package-module', '') is not None


def test_find_notifier_raises_exception_if_module_not_exists():
    with pytest.raises(FileNotFoundError):
        search.find_notifier('non-existent', 'notifier-a')


def test_find_notifier_raises_exception_if_notifier_not_exists():
    with pytest.raises(FileNotFoundError):
        search.find_notifier('file-module', 'non-existent')


def test_list_all_returns_all_notifier_names_grouped_by_module():
    assert search.list_all() == {
        'file-module': [
            'file-module', 'notifier-a', 'notifier-b'
        ],
        'package-module': [
            'failing-notifier', 'notifier-a', 'notifier-b', 'package-module'
        ],
    }

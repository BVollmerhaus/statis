"""
Test cases for the notification module.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

from unittest.mock import call

import pytest

from statis import notification as nf


@pytest.fixture(scope='module')
def notification() -> nf.Notification:
    return nf.Notification('Title', 'Body', action='action,label')


def test_send_invokes_dunst_correctly_if_available(mocker, notification):
    mocker.patch('statis.notification.shutil.which',
                 return_value='/usr/bin/dunstify')
    mocker.patch('statis.notification.subprocess.check_output',
                 return_value=b'dunst  knopwob  1.4.1 (2019-07-03)  1.2')

    run_mock = mocker.patch('statis.notification.subprocess.run')

    nf.send(notification, 5, nf.Urgency.NORMAL.value, 123456)
    notification.action = ''
    nf.send(notification, 5, nf.Urgency.NORMAL.value, 123456)

    expected_args = [
        'dunstify', 'Title', 'Body', '-i', '', '-t', '5000',
        '-u', 'normal', '-a', 'Statis', '--replace=123456',
        '--hints=string:category:device', '--action=action,label'
    ]

    run_mock.assert_has_calls([
        call(expected_args, check=True),
        call(expected_args[:-1], check=True),
    ])


def test_send_invokes_notify_send_correctly(mocker, notification):
    mocker.patch('statis.notification.shutil.which', return_value=None)
    run_mock = mocker.patch('statis.notification.subprocess.run')

    nf.send(notification, 5, nf.Urgency.NORMAL.value, 123456)

    run_mock.assert_called_once_with([
        'notify-send', 'Title', 'Body', '-i', '', '-t', '5000',
        '-u', 'normal', '-a', 'Statis', '-c', 'device'
    ], check=True)


@pytest.mark.parametrize('string, expected_id', [
    ('cpu usage', 215603),
    ('Cpu Usage', 215603),
    ('volume', 311393),
])
def test_id_from_string(string: str, expected_id: int):
    assert nf.id_from_string(string) == expected_id

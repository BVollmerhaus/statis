"""
Test cases for CLI parsing of monitoring thresholds.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import pytest

from statis import cli, monitor


def test_parse_args_sets_monitor_to_false_if_threshold_empty():
    args = cli.parse_args(['-m', '', 'module', 'notifier'])
    assert not args.monitor


@pytest.mark.parametrize('threshold', ['>=2GB', '>= 2 GB'],
                         ids=['no-spaces', 'spaces'])
def test_parse_args_builds_threshold_from_monitor_arg(threshold: str):
    args = cli.parse_args(['-m', threshold, 'module', 'notifier'])
    assert isinstance(args.monitor, monitor.Threshold)
    assert args.monitor.value == 2
    assert args.monitor.unit == 'GB'
    assert args.monitor.type == monitor.Threshold.Type.ABOVE_EQ


def test_parse_args_sets_threshold_type_to_above_if_missing():
    args = cli.parse_args(['-m', '2GB', 'module', 'notifier'])
    assert args.monitor.type == monitor.Threshold.Type.ABOVE


def test_parse_args_accepts_non_alphanumeric_threshold_unit():
    args = cli.parse_args(['-m', '50%', 'module', 'notifier'])
    assert args.monitor.unit == '%'


def test_parse_args_raises_exception_if_threshold_invalid():
    with pytest.raises(SystemExit):
        cli.parse_args(['-m', '?', 'module', 'notifier'])

"""
Test cases for the main module.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import sys
from unittest import mock

import pytest

from statis import __main__


@pytest.fixture(autouse=True)
def patch_search_paths(mocker, shared_datadir):
    """Replace module search paths with test data directory."""
    mocker.patch('statis.search.SEARCH_PATHS', [shared_datadir / 'modules'])


def test_main_finds_notifier_and_sends_notification(mocker):
    send_mock = mocker.patch('statis.notification.send')
    __main__.main(['-t', '2', '-u', 'normal', 'file-module', 'notifier-a'])
    send_mock.assert_called_once_with(mock.ANY, 2, 'normal', mock.ANY)


def test_main_uses_sys_argv_if_no_args_supplied(mocker):
    mocker.patch.object(sys, 'argv', ['statis', '-t', '2', '-u', 'normal',
                                      'file-module', 'notifier-a'])
    send_mock = mocker.patch('statis.notification.send')
    __main__.main()
    send_mock.assert_called_once_with(mock.ANY, 2, 'normal', mock.ANY)


def test_main_prints_notifier_list_if_list_flag_set(capsys):
    __main__.main(['--list'])
    out, _err = capsys.readouterr()
    assert out == ('file-module\n'
                   ' * file-module\n'
                   ' * notifier-a\n'
                   ' * notifier-b\n'
                   'package-module\n'
                   ' * failing-notifier\n'
                   ' * notifier-a\n'
                   ' * notifier-b\n'
                   ' * package-module\n')


def test_main_returns_66_noinput_if_notifier_not_exists():
    assert __main__.main(['non-existent', 'non-existent']) == 66


def test_main_returns_1_if_notifier_raises_exception():
    assert __main__.main(['package-module', 'failing-notifier']) == 1

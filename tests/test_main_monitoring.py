"""
Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

from typing import List, NoReturn

from statis import __main__
from statis.monitor import Monitor
from statis.notification import Notification
from statis.notifier import Notifier


class MockMonitor(Notifier, Monitor):
    def run(self) -> Notification:
        pass

    def monitor(self) -> NoReturn:
        pass

    def valid_threshold_units(self) -> List[str]:
        return ['GB']


def test_main_invokes_monitoring_if_monitor_arg_supplied(mocker):
    mock_monitor = MockMonitor()
    monitor_spy = mocker.spy(mock_monitor, 'monitor')
    mocker.patch('statis.search.find_notifier', return_value=mock_monitor)

    __main__.main(['-m', '2GB', 'not-relevant'])
    assert monitor_spy.call_count == 1


def test_main_returns_65_dataerr_if_specified_unit_unsupported(mocker):
    mocker.patch('statis.search.find_notifier', return_value=MockMonitor())
    assert __main__.main(['-m', '2 bad', 'not-relevant']) == 65


def test_main_returns_2_if_monitor_arg_supplied_to_non_monitor(mocker,
                                                               shared_datadir):
    mocker.patch('statis.search.SEARCH_PATHS', [shared_datadir / 'modules'])
    assert __main__.main(['-m', '2GB', 'file-module', 'notifier-a']) == 2

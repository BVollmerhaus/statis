"""
Test cases for parsing of command line arguments.

Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
License: MIT
"""

import logging

import pytest

from statis import cli


def test_parse_args_prints_help_if_no_module_or_list_flag_supplied(mocker):
    print_help_mock = mocker.patch('argparse.ArgumentParser.print_help')
    with pytest.raises(SystemExit):
        cli.parse_args([])
    assert print_help_mock.called


def test_parse_args_verbose_flag_sets_log_level():
    cli.parse_args(['--verbose', 'module', 'notifier'])
    assert logging.getLogger().getEffectiveLevel() == logging.INFO


def test_parse_args_debug_flag_sets_log_level():
    cli.parse_args(['--debug', 'module', 'notifier'])
    assert logging.getLogger().getEffectiveLevel() == logging.DEBUG


def test_parse_args_sets_remaining_args_as_notifier_args():
    args = cli.parse_args(['module', 'notifier', '-a', 'value'])
    assert args.notifier_args == ['-a', 'value']


def test_parse_args_distinguishes_base_and_notifier_args_if_identical():
    args = cli.parse_args(['-t', '42', 'module', 'notifier', '-t', 'value'])
    assert args.timeout == 42
    assert args.notifier_args == ['-t', 'value']
